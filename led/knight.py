#!/usr/bin/python

import RPi.GPIO as GPIO, Image, time, sys

def grb(colors):
	"""returns array slice for this LED
       Arguments:
         colors - 32 chars for 32 LEDs (per meter)
	"""
	t = bytearray(96+1) # each one needs 3 char each +1
	if len(colors) == 32:
		i = 0
		for col in list(colors):
			if col == 'R':
				t[i]=128
				t[i+1]=255
				t[i+2]=128
			elif col == 'r':
				t[i]=128
				t[i+1]=192
				t[i+2]=128
			elif col == 'G' or col == 'g':
				t[i]=255
				t[i+1]=128
				t[i+2]=128
			elif col == 'B' or col == 'b':
				t[i]=128
				t[i+1]=128
				t[i+2]=255
			elif col == 'W' or col == 'w':
				t[i]=255
				t[i+1]=255
				t[i+2]=255
			else:
				t[i]=128
				t[i+1]=128
				t[i+2]=128
			i+=3
		return t
	else:
		print "Error using this routine, see help"
		sys.error(1)

spidev    = file("/dev/spidev0.0", "wb") # Open SPI device
width     = 22 # could change, one for each "state" 
height    = 32 # probably never change, uses all LEDs in 1m strip

# R, G, B byte per pixel, plus extra '0' byte at end for latch.
print "Allocating..."
column = [0 for x in range(width)]
for x in range(width):
	column[x] = bytearray(height * 3 + 1)

# not GRB w/ white  = 255, and blk = 128

print "Setting states..."
column[0]  = grb("R...............................")
column[1]  = grb("rR..............................")
column[2]  = grb("rRR.............................")
column[3]  = grb("rRRR............................")
column[4]  = grb(".rRRR...........................")
column[5]  = grb("..rRRR..........................")
column[6]  = grb("...rRRR.........................")
column[7]  = grb("....rRRR........................")
column[8]  = grb(".....rRR........................")
column[9]  = grb("......rR........................")
column[10] = grb(".......r........................")
column[11] = grb(".......R........................")
column[12] = grb("......Rr........................")
column[13] = grb(".....RRr........................")
column[14] = grb("....RRRr........................")
column[15] = grb("...RRRr.........................")
column[16] = grb("..RRRr..........................")
column[17] = grb(".RRRr...........................")
column[18] = grb("RRRr............................")
column[19] = grb("RRr.............................")
column[20] = grb("Rr..............................")
column[21] = grb("r...............................")

print "Looping over all " + str(width) + " widths 10 times..."
#while True:
for y in range(10):
        for x in range(width):
                spidev.write(column[x])
                spidev.flush()
                time.sleep(.07)
column[0][0] = 128
column[0][1] = 128
spidev.write(column[0])
spidev.flush()
