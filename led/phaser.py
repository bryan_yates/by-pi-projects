#!/usr/bin/python

# Lights for shooting phaser, like in star wars

import RPi.GPIO as GPIO
# from PIL.Image import core as Image
import Image
import time
import os
import subprocess
from datetime import datetime

#import pygame

#pygame.mixer.init()

laserfiles = 'laser1good.mp3 laser2ok.mp3 laser3ok.mp3 laser4good.mp3 laser5good.mp3 laser6.mp3 laser7ok.mp3'.split()

#class Loopqueue:
#     def __init__(self):
#         self.items = []
#
#     def isEmpty(self):
#         return self.items == []
#
#     def rot(self, rots=1):
#         leftmost = self.pop()
#         self.items.pop()
#
#     def push(self, item):
#         self.items.insert(0,item)
#
#     def pop(self):
#         return self.items.pop()
#
#     def peek(self):
#         return self.items[len(self.items)-1]
#
#     def size(self):
#         return len(self.items)
#
#s=Stack()
#print(s.isEmpty())
#s.push(4)
#s.push('dog')
#print(s.peek())
#s.push(True)
#print(s.size())
#print(s.isEmpty())
#s.push(8.4)
#print(s.pop())
#print(s.pop())
#print(s.size())

#    #while pygame.mixer.music.get_busy() == True:
#    #    continue
#    pygame.mixer.music.load("/home/pi/Downloads/laser4.wav")
#    pygame.mixer.music.play()
#    #while pygame.mixer.music.get_busy() == True:
#    #    continue
#    pygame.mixer.music.load("/home/pi/Downloads/laser5.wav")
#    pygame.mixer.music.play()
#    #while pygame.mixer.music.get_busy() == True:
#    #    continue
#    pygame.mixer.music.load("/home/pi/Downloads/laser6.wav")
#    pygame.mixer.music.play()
#    #while pygame.mixer.music.get_busy() == True:
#    #    continue

GPIO.setmode(GPIO.BCM)
GPIO.setup(24, GPIO.IN)

# Configurable values
filename  = "/usr/share/adafruit/webide/repositories/by-pi-projects/led/images/phaserred.png" # red streaking
filename  = "/usr/share/adafruit/webide/repositories/by-pi-projects/led/images/phaserorange.png" # green streaking
filename  = "/usr/share/adafruit/webide/repositories/by-pi-projects/led/images/phasergreen.png" # green streaking
dev       = "/dev/spidev0.0"

# Open SPI device, load image in RGB format and get dimensions:
spidev    = file(dev, "wb")
img       = Image.open(filename).convert("RGB")
pixels    = img.load()
width     = img.size[0]
height    = img.size[1]

# Calculate gamma correction table.  This includes
# LPD8806-specific conversion (7-bit color w/high bit set).
gamma = bytearray(256)
for i in range(256):
	gamma[i] = 0x80 | int(pow(float(i) / 255.0, 2.5) * 127.0 + 0.5)

# Create list of bytearrays, one for each column of image.
# R, G, B byte per pixel, plus extra '0' byte at end for latch.
column = [0 for x in range(width)]
for x in range(width):
	column[x] = bytearray(height * 3 + 1)

# Convert 8-bit RGB image into column-wise GRB bytearray list.
for x in range(width):
    for y in range(height):
		value = pixels[x, y]
		y3 = y * 3
		column[x][y3]     = gamma[value[1]]
		column[x][y3 + 1] = gamma[value[0]]
		column[x][y3 + 2] = gamma[value[2]]

def nextlaserfile():
    laste = laserfiles.pop()
    laserfiles.insert(0,laste)
    return laste

def firephasers():
    os.system("mpg123 -q /home/pi/Downloads/%s &" % nextlaserfile() )
    time.sleep(.2)
    for x in range(width):
        spidev.write(column[x])
        time.sleep(0.01)
        spidev.flush()
        
def shutdown():
    for x in range(width-1,0,-1):
        spidev.write(column[x])
        time.sleep(0.1)
        spidev.flush()
    time.sleep(0.1)
    print "shutting down NOW"
    os.system("sudo poweroff")
# http://stackoverflow.com/questions/21328579/how-to-make-a-button-press-have-different-outputs-depending-on-time-pressed-in-p

## test to see how long it can run before the battery runs out
#rightnow = datetime.now()
#filedatestamp = rightnow.strftime('%Y%m%d_%H%M')
#
#f = open("/home/webide/repositories/by-pi-projects/led/phaserGo%s.txt" % (filedatestamp,), "w")
#f.write('%s' % (datetime.now(),) ) # Write a string to a file
#f.close()
#loop = 0        
#while True:
#    pressed_time = 0
#    loop += 1
#    firephasers()
#    if GPIO.input(24) == 0:
#        pressed_time = time.time() # time.monotonic()
#        while GPIO.input(24) == 0 and time.time()-pressed_time < 10.1: #call: is button still pressed, about 15 seconds and it will kick you out
#            pass
#        pressed_time = time.time() - pressed_time # time.monotonic()-pressed_time
#        # print "pushed button for %s sec?" % (pressed_time)
#    if pressed_time>=10:
#        # print "poweroff %s" % (pressed_time,)
#        shutdown()
#    if (loop % 100 == 0):
#        f = open("/home/webide/repositories/by-pi-projects/led/phaserFire%s.txt" % (filedatestamp,), "w")
#        f.write('loop %s at %s' % (loop,datetime.now() ) ) # Write a string to a file
#        f.close()
#        print "hi"
#    time.sleep(.00001)


# normal mode using a button
pressed_time = 0
while True and pressed_time < 10:
    pressed_time = 0
    if GPIO.input(24) == 0:
        pressed_time = time.time() # time.monotonic()
        firephasers()
        while GPIO.input(24) == 0 and time.time()-pressed_time < 10.1: #call: is button still pressed, about 15 seconds and it will kick you out
            pass
        pressed_time = time.time() - pressed_time # time.monotonic()-pressed_time
        # print "pushed button for %s sec?" % (pressed_time)
    if pressed_time>=10:
        # print "poweroff %s" % (pressed_time,)
        shutdown()
        
        
## push a key (interactive, with keyboard) rather than the button 
#pushed = ""
#while True and pushed <> "E":
#    pushed = raw_input("Fire? [Fe] ")
#    if pushed <> "E":
        firephasers()
