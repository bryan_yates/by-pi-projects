import RPi.GPIO as GPIO
import time
import os
import sys
# tb2g& https://stackoverflow.com/questions/20218570/how-to-determine-pid-of-process-started-via-os-system
import subprocess
import psutil


GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

switchBigOval = 27 # push to toggle live youtube stream
switchRound = 24 # push and hold for a sec, powers unit off

GPIO.setup(switchBigOval, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(switchRound, GPIO.IN, pull_up_down=GPIO.PUD_UP)

ledGreen = 22 # not sure what to do with this, try to use as power
ledRed = 23 # indciate on-air

GPIO.setup(ledGreen,GPIO.OUT)
GPIO.setup(ledRed,GPIO.OUT)
GPIO.output(ledGreen,GPIO.LOW)
GPIO.output(ledRed,GPIO.LOW)

# sys.exit()

tog1,tog2 = 1,1
startscript = 'raspivid --nopreview -md 4 -w 640 -h 480 -fps 15 -t 0 -b 200000 -g 30 -n -o - | /usr/src/ffmpeg/ffmpeg -y -xerror -thread_queue_size 32K -f h264 -r 15 -itsoffset 0 -i - -f alsa -ar 11025 -itsoffset 5.5 -async 1 -ac 1 -thread_queue_size 32K -i plughw:CARD=USBSA,DEV=0 -c:a aac -b:a 32k -async 1 -c:v copy -f flv -flags:v +global_header -rtmp_buffer 10000 -r 15 -async 1 rtmp://a.rtmp.youtube.com/live2/8srh-ar07-ujty-2w22'

def shutdown():
    GPIO.output(ledGreen,GPIO.LOW)
    GPIO.output(ledRed,GPIO.LOW)
    print "shutting down NOW"
    os.system("sudo poweroff")

def kill(proc_pid):
    process = psutil.Process(proc_pid)
    for proc in process.children(recursive=True):
        proc.kill()
    process.kill()

# one time startup LED sequence
for a in (range(120)):
    if a%2:
        if 1 == tog1:
            GPIO.output(ledRed,GPIO.HIGH)
        else:
            GPIO.output(ledRed,GPIO.LOW)
        tog1 = -tog1
    else:
        if 1 == tog2:
            GPIO.output(ledGreen,GPIO.HIGH)
        else:
            GPIO.output(ledGreen,GPIO.LOW)
        tog2 = -tog2
    time.sleep(.03)
print ("ok, final results are tog1={} and tog2={}".format(tog1,tog2))
    
while True:
    butt1 = GPIO.input(switchBigOval)
    if butt1 == False:
        pressed_time = time.time()
        print('switchBigOval Pressed at {}'.format(pressed_time) )
        if 1 == tog1:
            # was off, turn it on
            print ("turning on video")
            # os.system ("sudo /usr/share/adafruit/webide/repositories/by-pi-projects/led/startyoutubestream.sh")
            proc = subprocess.Popen(startscript,shell=True)
            # slow blink red to let me know it's coming up, for the 9 seconds it takes for audio to start, then leave ON
            for a in (range(180)):
                if a%3:
                    GPIO.output(ledRed,GPIO.HIGH)
                else:
                    GPIO.output(ledRed,GPIO.LOW)
                time.sleep(.05)
            # pid = proc.pid
            GPIO.output(ledRed,GPIO.HIGH)
        else:
            # turn off stream
            # print ("killing video")
            # this one failed with one or 2 pipes continuing on
            #os.system ("sudo killall startyoutubestream")
            # this one failed with both pipes still working
            # proc.terminate()
            # this worked, thanks to https://stackoverflow.com/questions/4789837/how-to-terminate-a-python-subprocess-launched-with-shell-true
            # blink backwards from soming up, for going off, for 11 seconds, then OFF
            for a in (range(222)):
                time.sleep(.05)
                if a%3:
                    GPIO.output(ledRed,GPIO.LOW)
                else:
                    GPIO.output(ledRed,GPIO.HIGH)
            kill(proc.pid)
            # print("about to blink red from {} to {} a hundred times or so".format(GPIO.LOW,GPIO.HIGH) )
            # print ("red should now be off after blinking")
        tog1 = -tog1
        while (not GPIO.input(switchBigOval)):
            pass
            #if time.time() - pressed_time > 20:
            #    shutdown()

    butt2 = GPIO.input(switchRound)
    if butt2 == False:
        pressed_time = time.time()
        # print('switchRound Pressed')
        if 1 == tog2:
            # tun on LED
            # print ('turn on ledGreen')
            GPIO.output(ledGreen,GPIO.HIGH)
        else:
            # turn off ledRed
            # print ('turn off ledGreen')
            GPIO.output(ledGreen,GPIO.LOW)
        tog2 = -tog2
        while (not GPIO.input(switchRound)):
            # print ("In inner loop now: {} then: {}".format(time.time(),pressed_time) )
            if time.time() - pressed_time > 10:
                # print ('Powering Down')
                # print("about to blink Green from {} to {} 20 times or so".format(GPIO.LOW,GPIO.HIGH) )
                for a in (range(21)):
                    time.sleep(.05)
                if a%2:
                    GPIO.output(ledGreen,GPIO.LOW)
                else:
                    GPIO.output(ledGreen,GPIO.HIGH)
                # print ("Green should now be ON after blinking, will shutdown kill it??")
                shutdown()
                
    time.sleep(0.1)