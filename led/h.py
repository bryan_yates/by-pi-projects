#!/usr/bin/python

# Lights for halloween, using 1 meter (32 LEDs) RGB strip

import RPi.GPIO as GPIO, Image, time
import os

c1=0 # always start with col 1, switch it around later
c2=0 # col 2 to show within each loop
poweroffflag=0 # guess what this triggers

GPIO.setmode(GPIO.BCM)
GPIO.setup(24, GPIO.IN)

# Configurable values
filename  = "./images/orangeblack.png" # solid blue, works
dev       = "/dev/spidev0.0"

# Open SPI device, load image in RGB format and get dimensions:
spidev    = file(dev, "wb")
img       = Image.open(filename).convert("RGB")
pixels    = img.load()
width     = img.size[0]
height    = img.size[1]

# Calculate gamma correction table.  This includes
# LPD8806-specific conversion (7-bit color w/high bit set).
gamma = bytearray(256)
for i in range(256):
	gamma[i] = 0x80 | int(pow(float(i) / 255.0, 2.5) * 127.0 + 0.5)

# Create list of bytearrays, one for each column of image.
# R, G, B byte per pixel, plus extra '0' byte at end for latch.
column = [0 for x in range(width)]
for x in range(width):
	column[x] = bytearray(height * 3 + 1)

# Convert 8-bit RGB image into column-wise GRB bytearray list.
for x in range(width):
	for y in range(height):
		value = pixels[x, y]
		y3 = y * 3
		column[x][y3]     = gamma[value[1]]
		column[x][y3 + 1] = gamma[value[0]]
		column[x][y3 + 2] = gamma[value[2]]

while (poweroffflag==0):
    for x in range(20):
        if (x==1):
            spidev.write(column[c1])
            spidev.flush()
        elif (x==11):
            spidev.write(column[c2])
            spidev.flush()
        if ( GPIO.input(24) == False ):
            if (c1==0 and c2==0): # solid, change to blink;
                c2=1
                spidev.write(column[c2])
                spidev.flush()
                x=11
            elif (c1==0 and c2==1): # blinking, change to off & poweroff
                c1=2
                c2=2
                spidev.write(column[c1])
                spidev.flush()
            elif (c1==2 and c2==2): # change from black back to solid, or poweroff
                time.sleep(.3) # let the button rest
                if ( GPIO.input(24) == False ): # holding button
                    poweroffflag=1
                else: # button not held, just restart loop
                    c1=0
                    c2=0
                    spidev.write(column[c1])
                    spidev.flush()
                    x=20
            time.sleep(.15)
        time.sleep(0.05)
print "Powering off"
os.system ("sudo poweroff")