#!/usr/bin/python

import thread
import time
#import termios, fcntl, sys, os
import msvcrt as m

# define the keyboard capture thread
#def grab_kinput():
#    fd = sys.stdin.fileno()
#    
#    oldterm = termios.tcgetattr(fd)
#    newattr = termios.tcgetattr(fd)
#    newattr[3] = newattr[3] & ~termios.ICANON & ~termios.ECHO
#    termios.tcsetattr(fd, termios.TCSANOW, newattr)
#    
#    oldflags = fcntl.fcntl(fd, fcntl.F_GETFL)
#    fcntl.fcntl(fd, fcntl.F_SETFL, oldflags | os.O_NONBLOCK)
#    c = "a"
#    
#    try:
#        while c <> 'q':
#            try:
#                c = sys.stdin.read(1)
#                print "Got character", repr(c)
#            except IOError: pass
#    finally:
#        termios.tcsetattr(fd, termios.TCSAFLUSH, oldterm)
#        fcntl.fcntl(fd, fcntl.F_SETFL, oldflags)

def grab_kinput():
    q = 0
    while q == 0:
        c = m.getch()
        if c:
            print "Button: %s pushed" % (c)
            if c == "q":
                q = 1

# Define a function for the thread
def print_time( threadName, delay):
   count = 0
   while count < 5:
      time.sleep(delay)
      count += 1
      print "%s: %s" % ( threadName, time.ctime(time.time()) )

# Create two threads as follows
try:
   thread.start_new_thread( grab_kinput, () )
   thread.start_new_thread( print_time, ("Thread-1", 2 ) )
   thread.start_new_thread( print_time, ("Thread-2", 3, ) )
except:
   print "Error: unable to start thread"

while 1:
   pass

