#!/usr/bin/env python

import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)

GPIO.setup(4, GPIO.IN, pull_up_down=GPIO.PUD_UP)

while True:
    input_state = GPIO.input(4)
    if input_state == False:
        print('Button Pressed')
        time.sleep(0.2)
        
# from time import sleep
# import os
# import RPi.GPIO as GPIO

# printcnt = 0;

# GPIO.setmode(GPIO.BCM)
# GPIO.setup(4, GPIO.IN)
 
# while True:
    # if ( GPIO.input(4) == False ):
    #  printcnt = printcnt + 1
    #  print "I see you" + str(printcnt);
    #  if (printcnt > 10):
    #      print "";
    #      printcnt = 0;
    #      # `sudo poweroff`;
    #sleep(0.1);
    
