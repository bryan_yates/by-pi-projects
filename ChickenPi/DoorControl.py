#!/usr/bin/python
import RPi.GPIO as GPIO
import time
import signal
import sys
import httplib, urllib #for Push Notifications

#config.txt included in .gitignore first line is the token, the second line is the key
# config = open('config.txt').readlines()
# pushover_token=config[0].rstrip()
# pushover_user=config[1]

#Setting up Board GPIO Pins
GPIO.setmode(GPIO.BOARD)
motorl = 35
motorr = 37
GPIO.setup(motorl,GPIO.OUT)
GPIO.setup(motorr,GPIO.OUT)

magswtop = 29
magswbottom = 33

# GPIO.setup(33,GPIO.IN)#Locked
# GPIO.setup(31,GPIO.IN)#Open
GPIO.setup(magswbottom, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Bottom/Locked
GPIO.setup(magswtop, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Top/Open

#Clean kill of script function (Stops Motor, cleans GPIO)
def Safe_Kill():
        print 'Performing safe shutoff!'
        GPIO.output(motorr,False)
        GPIO.output(motorl,False)
        GPIO.cleanup()
        sys.exit('Motors shutdown, GPIO cleaned')

#Argument controller
if len(sys.argv)>3: #Tests if you've entered too many arguments
    print "You've entered too many arguments!"
    print "Exiting program..."
    sys.exit(0)

if len(sys.argv)>2: #Argument for door action time
    try:
        float(sys.argv[2])
    except:
        print 'Error: ',str(sys.argv[2]),' is not a number!'
        print "Exiting program..."
        sys.exit(0)
    if int(sys.argv[2])>45: #Checks that a time longer than 45s isn't entered
            print 'Please choose a time less than 45s'
            print "Exiting program..."
            sys.exit(0)

if len(sys.argv)>1: #Argument for door action
    if sys.argv[1]!='close' and sys.argv[1]!='open':
            print 'Please choose "open" or "close"'
            print "Exiting program..."
            sys.exit(0)

if len(sys.argv)==3:
    print 'Forcing door to ',str(sys.argv[1]),' for ',str(sys.argv[2]),' seconds '
    Door_Action=sys.argv[1]
    Door_Time=int(sys.argv[2])
if len(sys.argv)==2:
    print 'Forcing door to ',str(sys.argv[1])
    Door_Action=sys.argv[1]
    Door_Time=45 #This is a safety time
if len(sys.argv)==1:
    Door_Action='default' #Will reverse door state
    Door_Time=45 #This is a safety time
 
#Start door!
#def DoorControl():
TimeStart=time.clock()
runTime=0
#Check door status from Magnets
BottomHall=GPIO.input(magswbottom)
TopHall=GPIO.input(magswtop)
if BottomHall==0:print 'Door is closed'
if TopHall==0:print 'Door is open'
#if BottomHall==1:print 'No magnet sensed on lock'
#if TopHall==1:print 'No magnet sensed top'
if Door_Action=='open': #Door is locked
		print 'The door is going up!'
		while TopHall==1 and runTime<Door_Time:
				GPIO.output(motorl,True)
				GPIO.output(motorr,False)
				TopHall=GPIO.input(magswtop)
				runTime=time.clock()-TimeStart
		if 45==runTime:
				print 'Something went wrong, go check the door!'
				message = 'Coop open FAILED!'
				print message
				Safe_Kill()
		if TopHall==0:
				print 'Door openend'
				# put date, time, and opened status on webpage
				Safe_Kill()
elif Door_Action=='close': #Door is open
		print 'The door is going down!'
		while BottomHall==1 and runTime<Door_Time:
				GPIO.output(motorl,False)
				GPIO.output(motorr,True)
				BottomHall=GPIO.input(magswbottom)
				runTime=time.clock()-TimeStart
		if 45==runTime:
				print 'Something went wrong, go check the door!'
				message = "Coop close FAILED!"
				print message
				Safe_Kill()
		if BottomHall==0:
				time.sleep(1)
				print 'Door is locked!'
				# put date, time, and closed status on webpage
				Safe_Kill()
elif BottomHall==0: #Door is locked
		print 'The door is locked!'
		print 'The door is going up!'
		while TopHall==1 and runTime<Door_Time:
				GPIO.output(motorl,True)
				GPIO.output(motorr,False)
				TopHall=GPIO.input(magswtop)
				runTime=time.clock()-TimeStart
		if 45==runTime:
				print 'Something went wrong, go check the door!'
				message = "Coop open FAILED!"
				print message
				Safe_Kill()
		if TopHall==0:
				print 'Door is open!'
				message = "Coop opened successfully!"
				print message
				Safe_Kill()
elif TopHall==0: #Door is open
		print 'The door is open!'
		print 'The door is going down!'
		while BottomHall==1 and runTime<Door_Time:
				GPIO.output(motorl,False)
				GPIO.output(motorr,True)
				BottomHall=GPIO.input(magswbottom)
				runTime=time.clock()-TimeStart
		if 45==runTime:
				print 'Something went wrong, go check the door!'
				message = "Coop close FAILED!"
				print message
				Safe_Kill()
		if BottomHall==0:
				print 'Door is locked!'
				message = "Coop closed successfully!"
				print message
				Safe_Kill()
runTime=time.clock()-TimeStart
print 'Total Time: '+str(runTime)
Safe_Kill()
