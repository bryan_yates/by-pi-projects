#!/usr/bin/python
import RPi.GPIO as GPIO
import time
import signal
import sys
import os

# import httplib, urllib #for Push Notifications

# loop forever looking for a change in the manual switch
# If physical switch changed from middle to up, go up
# If physcial switch changed from middle to down, go down
# any other combination discarded
# do the same with a virtual switch from the webpage

#Setting up Board GPIO Pins
GPIO.setmode(GPIO.BOARD)
SWITCH_UP = 38
SWITCH_DOWN= 40
chickenstatusfile = '/var/www/html/chickenstatus.txt'

GPIO.setup(SWITCH_UP, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(SWITCH_DOWN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
lastup = 0
lastdown = 0
#statbuf = os.stat(chickenstatusfile)
lastfileupdatetime = 1
#statbuf.st_mtime
    
# Create functions to run when the buttons are pressed
#def Up(channel):
#    # Put whatever Button 1 does in here
#    print 'Up switch hit';
#   
#def Down(channel):
#    # Put whatever Button 2 does in here
#    print 'Down switch hit';
#
## Wait for Button 1 to be pressed, run the function in "callback" when it does, also software debounce for 300 ms to avoid triggering it multiple times a second
#GPIO.add_event_detect(SWITCH_UP, GPIO.BOTH, callback=Up, bouncetime=300)
#GPIO.add_event_detect(SWITCH_DOWN, GPIO.BOTH, callback=Down, bouncetime=300) # Wait for Button 2 to be pressed

# Start a loop that never ends
while True:
    # text file options are:

    # Please open the door
    # Please close the door
    # Door is closed
    # Door opening
    # Door is open
    # Door is closing
    statbuf = os.stat(chickenstatusfile)
    newfileupdatetime = statbuf.st_mtime 
    
    if newfileupdatetime > lastfileupdatetime:
        # open file and get a command
        target = open(chickenstatusfile, 'r')
        textstatus = target.read()
        target.close()
        if textstatus.startswith('Please'):
            if textstatus.startswith('close', 7):
                print "Close the door, fool"
            else:
                print "Open the door, fool"
            time.sleep(30)
    else:
        # ok, the text file is a wash, let's see if the switch changed 
        newup = GPIO.input(SWITCH_UP)
        newdown = GPIO.input(SWITCH_DOWN)
        if newup == 1 and lastup == 0:
            lastup = 1
            print "Open the door, fool2"
            time.sleep(30)
        elif newdown == 1 and lastdown == 0:
            lastdown = 1
            print "Close the door, fool2"   
            time.sleep(30)

    # message = 'Updated door thing ' 
    # target.write(message + time.strftime("%x") + ' ' +  time.strftime("%X") + "\n")
    time.sleep(5)