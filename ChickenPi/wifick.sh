#!/bin/bash
# Purpose: to check if the internet is up (via ping test to google)
# and if not, restart networking service via ifdown and ifup

#check if there is internet via ping test
if ! [ "`ping -c 1 google.com`" ]; then #if ping exits nonzero...
  /sbin/ifdown wlan0 && /sbin/ifup wlan0
fi
